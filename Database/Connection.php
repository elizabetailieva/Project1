<?php

namespace MyProject;

use \PDO;

class DB {
	public static function connect() {
		try {
			if(isset($pdo) && $pdo instanceof PDO) {
				return $pdo;
			}

			$pdo = new PDO('mysql:host=localhost;dbname=project', 'root', 'root');
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			return $pdo;
		} catch (Exception $e) {
			return false;
		}
	}
}
