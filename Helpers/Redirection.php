<?php

namespace MyProject;

class Redirection {
	public static function redirect($to, $queryString = '', $scrollTo = '') {
		header("Location: $to?".implode("&", $queryString).$scrollTo);
		die();
	}
}