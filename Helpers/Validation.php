<?php

namespace MyProject;

class Validation {

	public static function validateLogin($username, $password) {
		$pdo = DB::connect();

		$sql = "SELECT * FROM admin WHERE username = :username AND password = :pass";

		$stmt = $pdo->prepare($sql);
		$stmt->bindParam('username', $username);
		$stmt->bindParam('pass', md5($password));

		$stmt->execute();

		if($stmt->rowCount() == 1) {
			return true;
		} else {
			return false;   
		}
	}

	public static function validateEmail($email) {
		if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return true;
		}
		return false;
	}

	public static function validateDescription($description) {
		if (strlen($description) <= 200) {
			return true;
		} else {
			return false;
		}
	}

}