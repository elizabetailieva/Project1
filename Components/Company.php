<?php

namespace MyProject;

require_once 'Helpers/Validation.php';

class Company {
	private $company_name, $email, $phone;

	public function __construct($company_name, $email, $phone ){
		$this->company_name = $company_name;
        $this->email = $email;
        $this->phone = $phone;
	}
	
	public function save(){
		$db = DB::connect();
		if (Validation::validateEmail($this->email)) {
			try {
				$sql = "INSERT INTO company (company_name, email, phone) VALUES (?, ?, ?)";
				$stmt = $db->prepare($sql);
				return $stmt->execute([$this->company_name, $this->email, $this->phone]);
			} catch (Exception $e) {
				Redirection::redirect('index.php');
				return false;
			}
		} else {
			Redirection::redirect('index.php', ['status=sentErrorEmail']);
		}
	}

}