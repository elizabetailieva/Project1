<nav class="navbar-default yellow-background">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button hamburger-wrap" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <i class="fa fa-bars my-bars"></i>			
            </button>
            <a class="small-logo" href="#"><img src="public/img/brainster_logo.png" class="logo img-responsive"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="my-list nav navbar-nav navbar-right admin-navbar-right">
                <li class="admin-nav-item">
                    <form method="POST" action="admin_project_insert.php">
                        <input type="hidden" name="session" value="successLogin">
                        <button type="submit" class="btn-admin-head">Внеси нов проект</button>
                    </form>
                </li>
                <li class="admin-nav-item">
                    <a href="index.php"><button class="btn-admin-head">Одјави се</button></a>
                </li>
            </ul>
        </div>
    </div>
</nav>