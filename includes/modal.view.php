<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content custom-modal">
            <div class="modal-header">
                <button type="button" class="close close-style" data-dismiss="modal">&times;</button>
                <h3>Вработи наши студенти </h3>
            </div>
            <div class="modal-body ">
                <form method="POST" action="edit.php">
                    <div class="form-group">
                        <input required type="text" name="company_name" class="input" placeholder="Име на компанија"/>
                    </div>				
                    <div class="form-group">
                        <input required type="email" name="email" class="input" placeholder="Контакт email"/>
                    </div>
                    <div class="form-group">
                        <input required type="tel" name="phone" class="input" placeholder="Телефонски број"/>
                    </div>
                    <div class="btn-block">
                        <input type="hidden" name="method" value="company"/>
                        <input type="submit" class="input input-btn" value="Прати"> 
                    </div>        
                </form>
            </div>
        </div>
    </div>
</div>