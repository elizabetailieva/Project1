<?php
namespace MyProject;
require_once "Database/Connection.php";
require_once "Components/Project.php";
$stmt = Project::getAll();
?> 

<!DOCTYPE html>
<html>

	<head>
		<?php
			include_once "includes/head.view.php";
		?>
	</head>

	<body>

		<?php
			include_once "includes/navbar.view.php";
			include_once "includes/modal.view.php";

			
		?>



		<section class="intro">
			<div class="cover">
				<div class="container heading">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="text-centered text-white text-padding">

<?php
	if(isset($_GET['status'])) {
		if($_GET['status']=='sent') {
			echo "<h3 class='yellow'>Ви благодариме за Вашиот интерес!</h3>";
		} elseif($_GET['status']=='sentError') {
			echo "<h4 class='yellow'>Настана грешка при праќање на Вашите податоци...</h4>";
		} elseif($_GET['status']=='sentErrorEmail') {
			echo "<h4 class='yellow'>Внесете валиден e-mail формат...</h4>";
		}
	}
?>
								<h1>Brainster.xyz Labs</h1>
								<h4>Проекти на студентите на академиите за програмирање и маркетинг на Brainster</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
				<div class="container projects">
					<div class="row">
						<?php	
							while($row = $stmt->fetch()) {	
						?>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="project-card">
								<a href="<?= $row['link'] ?>" target="_blank" class="unstyled-link">
									<div class="project-image">
										<img src="<?= $row['img_path'] ?>" class="project-logo">
									</div>
								</a>
								<div class="project-name">
									<h2><?= $row['title'] ?></h2>
									<h4><?= $row['subtitle'] ?></h4>
								</div>
								<div class="project-description">
									<p><?= $row['description'] ?></p>
								</div>
							</div>		
						</div>
						<?php
							}
						?>
					</div>
				</div>
		</section>

		<?php
			include_once "includes/footer.view.php";
		?>

		<script src="scripts/jquery.js"></script>
		<script src="scripts/bootstrap.js"></script>

	</body>

</html>