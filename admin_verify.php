<?php

require_once 'Database/Connection.php';
require_once 'Helpers/Validation.php';
require_once 'Helpers/Redirection.php';

use MyProject\Database\Connection;
use MyProject\Validation;
use MyProject\Redirection;
use MyProject\Admin;

$username = $_POST['username'];
$password = $_POST['password'];


if(Validation::validateLogin($username, $password)) {
		Redirection::redirect('admin_panel.php', ['status=successLogin']);
} else {
		Redirection::redirect('admin.php', ['status=errorLogin']);
}


