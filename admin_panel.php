<?php

namespace MyProject;

require_once 'Helpers/Redirection.php';
require_once "Database/Connection.php";
require_once "Components/Project.php";
$stmt = Project::getAll();


if (isset($_GET['status']) && $_GET['status'] != 'errorLogin') {
?>

<!DOCTYPE html>
    <html>

    <head>
        <?php
        include_once "includes/head.view.php";
        ?>
    </head>

    <body>

        <?php
        include_once "includes/navbar.admin.view.php";
        include_once "includes/msg.admin.panel.php";
        ?> 
        <div class="container">
            <div class="row">
                <?php	
                    while ($row = $stmt->fetch()) {
                ?>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="project-card">
                            <form method="post" action="edit.php">
                                <a href="<?= $row['link'] ?>" target="_blank" />
                                    <div class="project-image">
                                        <img src="<?= $row['img_path'] ?>" class=" img-responsive project-logo">
                                    </div>
                                </a>
                                <div class="project-name">
                                    <h2><?= $row['title'] ?></h2>
                                    <h4><?= $row['subtitle'] ?></h4>
                                </div>
                                <div class="project-description admin-desc">
                                    <p><?= $row['description'] ?></p>
                                </div>
                                <div class="buttons">
                                    <button class="admin-button btn-left" type="submit" name="update" value="<?= $row['id'] ?>"><i class="fas fa-edit"></i>Уреди</button>
                                    <button class="admin-button btn-right" type="submit" name="delete" value="<?= $row['id'] ?>"><i class="fas fa-trash-alt"></i>Избриши</button>
                                </div>
                                <div class="clear-float">
                                </div>
                            </form>
                        </div>
                    </div>     
                <?php
                    }
                ?>
            </div>
        </div>
    
        <?php
        include_once "includes/footer.view.php";
        ?>

        <script src="scripts/jquery.js"></script>
        <script src="scripts/bootstrap.js"></script>
    </body>

    </html>
<?php

} else {
    Redirection::redirect('index.php');
    die();
}